import mongoose from 'mongoose'
import dotenv from 'dotenv'
dotenv.config();
const mongodb_url = process.env.MONGODB_URL
console.log(mongodb_url);
if(!mongodb_url){
    mongoose.connect("mongodb://0.0.0.0:27017/todoapp")
}
else{
    mongoose.connect(mongodb_url)
}

mongoose.connection.on('error', err => {
    console.log("kết nối lỗi với db " + err);
})
mongoose.connection.on('connected', res => {
    console.log("Kết nối thành công với db");
})