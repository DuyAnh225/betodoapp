import mongoose from "mongoose";
const Schema = mongoose.Schema

const todoSchema = new Schema({
    name:{
        type:String,
        require: true
    },
    description:{
        type:String,
        require:true
    },
    status:{
        type:Boolean,
        require:true
    },
},
{timestamps:true}
)

export const toDoModel = mongoose.model("todos",todoSchema);
export const getAllToDo =()=> toDoModel.find();
export const getToDoById =(id:string)=>toDoModel.findById(id);
export const createToDo=(values:Record<string,any>)=>new toDoModel(values).save().then((todo)=>todo.toObject());
export const updateToDoById = (id:string,values : Record<string,any>)=>toDoModel.findByIdAndUpdate(id,values);
export const deleteToDoById =(id:string)=>toDoModel.findByIdAndDelete({_id:id});
