import express from 'express'

import {getListToDo,creToDo, updateToDo, delToDo} from '../controllers/todo'

export default(router:express.Router)=>{
    router.get('/list',getListToDo)
    router.post('/add',creToDo)
    router.put('/update/:id',updateToDo)
    router.delete('/delete/:id',delToDo)
}