import express from 'express'
import cors from 'cors'
import dotenv from 'dotenv'
import './config/db'
import router from './routes'
dotenv.config();

const app = express();
const port = process.env.PORT;

app.use(express.json());
app.use(cors({
    credentials:true
}))

app.use('/',router());


const server = app.listen(port,()=>{
    console.log("server is running at "+port);
})