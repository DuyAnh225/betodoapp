import express, { response } from 'express'

import {getAllToDo,createToDo,updateToDoById,getToDoById,deleteToDoById} from '../models/todo'


export const creToDo = async (req:express.Request,res:express.Response)=>{
    try{
        const {name,description,status}= req.body;
        if(!name || !description){
            return res.status(400).json("Không được để trong dữ liệu");
           
        }
        else{
            const tmp = await createToDo({
                name,
                description,
                status
            });
            return res.status(200).json({
                message:"Thêm thành công",
                data:tmp
            }).end();
        }
   }
    catch(error){
        console.log(error);
        return   res.status(400).json("Lỗi create todo ")
    }
}
export const getListToDo = async(req:express.Request,res:express.Response)=>{
    try{
        const lst = await getAllToDo();
        return res.status(200).json(lst).end();
           

    }
    catch(error){
        console.log(error);
      return   res.status(400).json("Lỗi get all todo")
    }
}
export const updateToDo = async(req:express.Request,res:express.Response)=>{
    try{
        const {id}= req.params;
        const body = req.body;
        
            const todo = await updateToDoById(id,body);
            res.status(200).json({
                message:"update thành công",
                todo:todo
            })
            
        
    }
    catch(error){
        console.log(error);
        return res.status(400).json("Lỗi update")
    }
}
export const delToDo = async(req:express.Request,res:express.Response)=>{
    try{
        const {id} = req.params;
        const deleteToDo = await deleteToDoById(id);
        res.status(200).json({
            message:"Xoá thành công",
            todo: deleteToDo
        })
    }catch(error){
        console.log(error);
        return res.status(400).json("Lối delete")
    }
}

